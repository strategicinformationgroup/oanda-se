/* xxcurroanda.p - Download Oanda Currency Exchange Rates                                               

   09/02/2014, Dumitru Cotfas, Strategic Information Group

*/

{us/bbi/mfdeclre.i}
{us/xx/xxscc.i}

/*Oanda download variable*/
DEFINE SHARED VARIABLE filPath        AS CHARACTER   NO-UNDO LABEL "Folder".
DEFINE SHARED VARIABLE voandakey      AS CHARACTER   NO-UNDO.
DEFINE SHARED VARIABLE voandaapi      AS CHARACTER   NO-UNDO.
DEFINE SHARED VARIABLE voandalog      AS CHARACTER   NO-UNDO.

DEFINE SHARED VARIABLE v_date         as date        no-undo.
DEFINE SHARED VARIABLE v_time         as integer     no-undo.
DEFINE SHARED VARIABLE v_dn_date      as date        no-undo LABEL "Exch Rate Date".

DEFINE VARIABLE v_genlog              AS CHARACTER   NO-UNDO.
DEFINE VARIABLE v_logtitle            AS CHARACTER   NO-UNDO.
DEFINE VARIABLE v_logentry            AS CHARACTER   NO-UNDO.
DEFINE VARIABLE v_logdate             AS CHARACTER   NO-UNDO.
DEFINE VARIABLE v_loggen              AS CHARACTER   NO-UNDO.
DEFINE VARIABLE v_ratescmd            AS CHARACTER   NO-UNDO.
DEFINE VARIABLE v_xmlfile             AS CHARACTER   NO-UNDO.
DEFINE VARIABLE v_xmlmore             AS CHARACTER   NO-UNDO.

DEFINE STREAM outcmd.

DEFINE SHARED TEMP-TABLE tcurrlist   
    FIELD tnbr        AS INTEGER
    FIELD tbasecurr   AS CHARACTER 
    FIELD texchcurr   AS CHARACTER
    FIELD texchfile   AS CHARACTER
    FIELD terror      AS CHARACTER 
    .

v_logtitle = "echo " + """" + "GENERAL LOG ENTRY -- " + STRING(TODAY) + """"  + " > " + voandalog.
OS-COMMAND SILENT VALUE (v_logtitle). 


v_logentry   = "echo " + """" + "OANDA CURRECNY DOWNLOAD" + """"  + " >> " + voandalog.
v_logdate    = "date >> " + voandalog.
v_genlog      = SUBSTRING(voandalog, 1, INDEX(voandalog, ".") - 1) + "_global.log".

FOR EACH tcurrlist:

    v_ratescmd = 
          "curl -X GET -H " 
          + """" + "Authorization: Bearer " 
          + voandakey 
          + """" 
          + " " 
          + """" 
          + voandaapi
          + TRIM(texchcurr)          
          + ".xml?quote=" 
          + TRIM(tbasecurr)
          + "&date="
          + string(year(v_dn_date),"9999") + "-"
          + string(month(v_dn_date),"99") + "-"
          + string(day(v_dn_date),"99")          
          + """"  
          + " > "
          + filPath 
          + "oanda_"
          + TRIM(texchcurr)          
          + "_"
          + TRIM(tbasecurr)
          + "_"
          + string(year(v_date),"9999")
          + string(month(v_date),"99")
          + string(day(v_date),"99")
          + replace(string(v_time,"HH:MM:SS"),":","")           
          + ".xml"                  
          + " "
          + "2>> "
          + voandalog
          .
          
    texchfile =  
          filPath 
          + "oanda_"
          + TRIM(texchcurr)          
          + "_"
          + TRIM(tbasecurr)
          + "_"
          + string(year(v_date),"9999")
          + string(month(v_date),"99")
          + string(day(v_date),"99")
          + replace(string(v_time,"HH:MM:SS"),":","")           
          + ".xml".
 
    v_xmlfile = "echo " + texchfile  + " >> " + voandalog.  
    v_xmlmore = "more " + texchfile  + " >> " + voandalog.    

    OS-COMMAND SILENT VALUE (v_logentry). 
    OS-COMMAND SILENT VALUE (v_logdate).  
    OS-COMMAND SILENT VALUE (v_xmlfile).
    OS-COMMAND SILENT VALUE (v_ratescmd).  
    OS-COMMAND SILENT VALUE (v_xmlmore). 
    

                  
END. /* FOR EACH tcurrlist:  */

 v_xmlmore = "more " + voandalog  + " >> " + v_genlog. 
 OS-COMMAND SILENT VALUE (v_xmlmore). 

/*******
TEMPLATE
  v_rates = "curl -X GET -H " 
          + """" + "Authorization: Bearer <OANDA_KEY>" + """" 
          + " " 
          + """" + "https://www.oanda.com/rates/api/v1/rates/USD.xml?quote=EUR&quote=CAD" + """"
          + " > /home/mfg/dco/rate3.xml 2>> /home/mfg/dco/rate3.log".
**********/

 
         
                     
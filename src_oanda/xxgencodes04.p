def var q as char format "x" init '"'.
def var v_domain as char.
def var v_fldname as char.
def var v_file as char.

{us/bbi/mfdeclre.i}

v_fldname = "OANDA_EXCH_RATE".
v_file = "oanda_settings.cim".

output to value("/home/mfg/dco/" + v_file).

for each code_mstr no-lock
where code_domain = global_domain
and code_fldname = v_fldname
:

   put unformatted
      "@@batchload mgcodemt.p"  skip
      q code_fldname q skip
      q code_value q skip
      q code_cmmt q skip
      "." skip
      "@@end" skip
   .

end.

output close.

/* -----------------------------------------------------------------------
   xxscc.i
        Strategic
        Shipment Compliance Controls shared code

        Paul Bonn, Strategic Information Group, 2013-Feb-05
   ----------------------------------------------------------------------- */

&GLOBAL-DEFINE      scc             "XX_SCC"
&GLOBAL-DEFINE      exempt-types    "EXEMPT_TYPES"
&GLOBAL-DEFINE      lic-auth        "AUTHORITY_CODES"
&GLOBAL-DEFINE      scc-enable      "ENABLE"
&GLOBAL-DEFINE      generic-enable  "GENERIC_ENABLE"
&GLOBAL-DEFINE      scc-err         "ERR_MSG_NBR"
&GLOBAL-DEFINE      scc-warn        "WARN_MSG_NBR"
&GLOBAL-DEFINE      load-directory  "LOAD_DIRECTORY"

/* Uses the following custom message records for errors and warnings:

    9xx01    - Item not authorized for sales to ship-to country.
    9xx02    - Ship-to country not authorized.  Continue?
*/

define variable xx-scc-err-msg-nbr      like msg_mstr.msg_nbr.
define variable xx-scc-warn-msg-nbr     like msg_mstr.msg_nbr.

/* -----------------------------------------------------------------------
   f-gencode-lkup returns character
        Perform generalized code lookup

        Paul Bonn, Strategic Information Group, 2013-Feb-05
   ----------------------------------------------------------------------- */
function f-gencode-lkup returns character
        (code-fld as character, code-val as character):

    define variable ret-val as character.

    for first code_mstr no-lock
            where code_domain  = global_domain
              and code_fldname = code-fld
              and code_value   = code-val
            :
        ret-val = code_cmmt.
    end.    /* for first code_mstr */

    return ret-val.
end function.      /* f-gencode-lkup */
/* ----------------------------------------------------------------------- */

xx-scc-err-msg-nbr = integer(f-gencode-lkup({&scc}, {&scc-err})).
if xx-scc-err-msg-nbr = 0 then
    xx-scc-err-msg-nbr  = 90101.

xx-scc-warn-msg-nbr = integer(f-gencode-lkup({&scc}, {&scc-warn})).
if xx-scc-warn-msg-nbr = 0 then
    xx-scc-warn-msg-nbr = 90102.



/* xxexchqdoc.p  Program to create the Qdoc  -                        */ 
/* SIG Created by  BY: Dumitru Cotfas - DCO       DATE: 08/14/12      */

{us/bbi/mfdeclre.i}
{us/xx/xxscc.i}

/*CSV file read variables */

DEFINE SHARED VARIABLE v_workdir       AS CHARACTER   NO-UNDO.
DEFINE SHARED VARIABLE v_qdocwebservice AS CHARACTER NO-UNDO.
DEFINE SHARED VARIABLE filDataset      AS CHARACTER   NO-UNDO. 
DEFINE SHARED VARIABLE maillist        AS CHARACTER  NO-UNDO.
/*
DEFINE VARIABLE filPath        AS CHARACTER   NO-UNDO.
DEFINE VARIABLE filPrefix      AS CHARACTER   NO-UNDO.
DEFINE VARIABLE filType        AS CHARACTER   NO-UNDO.
DEFINE VARIABLE chImport       AS CHARACTER   NO-UNDO.
DEFINE VARIABLE vfile1         AS CHARACTER   NO-UNDO.
DEFINE VARIABLE vfile2         AS CHARACTER   NO-UNDO. 

define variable hDoc as handle no-undo.
define variable hLine as handle no-undo.
define variable hField as handle no-undo.
define variable v_qdoc_req as character no-undo.  /* Qxtend Qdoc request file*/
DEFINE VARIABLE v_type         as CHARACTER   no-undo.
*/

DEFINE SHARED VARIABLE v_date          as date        no-undo.
DEFINE SHARED VARIABLE v_time          as integer     no-undo.
DEFINE VARIABLE v_datetime AS CHARACTER NO-UNDO. 
DEFINE VARIABLE v_oscmd AS CHARACTER    NO-UNDO.

/*DEFINE SHARED VARIABLE v_qxq_dir       as character no-undo. */  /* QXtend Queue drop dir */
DEFINE SHARED VARIABLE v_exch_templ    as character no-undo.  /* Qxtend Qdoc templates */

define variable v_longTemplate as longchar no-undo.
define variable v_longReplace as longchar no-undo.
DEFINE VARIABLE v_reqfile AS CHARACTER. 
DEFINE VARIABLE v_responsefile AS CHARACTER. 
DEFINE VARIABLE v_logfile AS CHARACTER. 
DEFINE VARIABLE v_qdocroot AS CHARACTER NO-UNDO.

/*
DEFINE SHARED TEMP-TABLE exchtb
   FIELD exch_ID                  AS INTEGER
   FIELD exch_filename            AS CHARACTER     
   FIELD exch_currfrom            AS CHARACTER     
   FIELD exch_currto              AS CHARACTER    
   FIELD exch_datetext            AS CHARACTER   
   FIELD exch_datestart           AS CHARACTER   
   FIELD exch_dateto              AS CHARACTER   
   FIELD exch_rate1               AS DECIMAL
   FIELD exch_rate2               AS DECIMAL 
   FIELD exch_request_time        AS CHARACTER  
   FIELD exch_response_code       AS CHARACTER
   FIELD exch_response-message    AS CHARACTER
   FIELD exch_load                AS LOGICAL 
   FIELD exch_exists              AS LOGICAL 
   FIELD exch_admemail            AS LOGICAL 
   FIELD exch_comment             AS CHARACTER  
   .
*/

DEFINE SHARED TEMP-TABLE exchtb
   FIELD exch_ID                  AS INTEGER
   FIELD exch_filename            AS CHARACTER     
   FIELD exch_currfrom            AS CHARACTER     
   FIELD exch_currto              AS CHARACTER    
   FIELD exch_datetext            AS CHARACTER   
   FIELD exch_validfrom           AS CHARACTER   
   FIELD exch_dateto              AS CHARACTER   
   FIELD exch_rate1               AS DECIMAL
   FIELD exch_rate2               AS DECIMAL 
   FIELD exch_request_time        AS CHARACTER  
   FIELD exch_response_code       AS CHARACTER
   FIELD exch_response-message    AS CHARACTER
   FIELD exch_load                AS LOGICAL 
   FIELD exch_exists              AS LOGICAL 
   FIELD exch_admemail            AS LOGICAL 
   FIELD exch_comment             AS CHARACTER  
   .   
    
function f_add_slash returns character
   (input fv_dir as character) forward.

function f_get_code_mstr returns character
   (input fv_fld as character, input fv_value as character) forward.
   
FUNCTION qdoc_date RETURNS CHARACTER    
  (input indate as date) FORWARD.   
  
define variable hReq as handle no-undo.
create x-document hReq.   

v_qdocroot = "BExchangeRate".
    

  FOR EACH exchtb WHERE exch_load = TRUE :  
  
                      v_date = TODAY.
                      v_time = TIME.
                      v_datetime = string(year(v_date),"9999")
                                 + string(month(v_date),"99")
                                 + string(day(v_date),"99")
                                 + replace(string(v_time,"HH:MM:SS"),":",""). 
                         
                      /* CREATE THE */
                      /* Load the QXI QDOC Template */
                      hReq:load("file",v_exch_templ,false).
                      hReq:save("longchar", v_longTemplate).  
                      
                      
                      /* Replace Date From  */
                      v_longReplace = REPLACE(v_longTemplate, "DATE_FROM", exch_validfrom).
                      v_longTemplate = v_longReplace.
                      
                      /* Replace   */
                      v_longReplace = REPLACE(v_longTemplate, "DATE_TILL", exch_dateto).
                      v_longTemplate = v_longReplace.    
                      
                      /* Replace   */
                      v_longReplace = REPLACE(v_longTemplate, "FROM_CURRENCY", exch_currfrom).
                      v_longTemplate = v_longReplace.    
                      
                      /* Replace   */
                      v_longReplace = REPLACE(v_longTemplate, "TO_CURRENCY", exch_currto).
                      v_longTemplate = v_longReplace.    
                      
                      /* Replace   */
                      v_longReplace = REPLACE(v_longTemplate, "EXCHANGE_RATE", STRING(exch_rate2)).
                      v_longTemplate = v_longReplace.                          
                      
                      v_reqfile      = v_workdir + v_qdocroot + v_datetime + ".req".
                      v_responsefile = v_workdir + "response_" + TRIM(v_qdocroot) + v_datetime  + ".xml".
                      v_logfile      = v_workdir + "logwebserv" + v_datetime + ".log".     
                      
                      
                      
                      COPY-LOB OBJECT v_longTemplate TO FILE v_reqfile. 
                      
                      /* Webservice Call */
                        assign
                          v_oscmd = "curl --url " + v_qdocwebservice + " -X POST"
                                  + " -H "   + """" + "Content-type: text/xml" + """" 
                                  + " -H "   + """" + "SOAPAction: ''" + """"
                                  + " -T "   + v_reqfile 
                                  + " > "    + v_responsefile
                                  + " 2>> "  + v_logfile
                                  .  

                        OS-COMMAND SILENT VALUE(v_oscmd).  
                        
                        IF filDataset <> "yes" THEN DO:
                              /*  files cleanup */
                              v_oscmd = "rm " + v_reqfile.
                              OS-COMMAND SILENT VALUE (v_oscmd) . 
                              v_oscmd = "rm " + v_responsefile.
                              OS-COMMAND SILENT VALUE (v_oscmd) .             
                              v_oscmd = "rm " + v_logfile .
                              OS-COMMAND SILENT VALUE (v_oscmd) .   

                        END.                        
                      
                      
  
  END. /* FOR EACH exchtb   ***/

/*CLEAN UP*/
delete object hReq.  



/************PROCEDURES****************/
procedure p_attach_line:
   define input parameter hParent as handle no-undo.
   define input parameter hLine as handle no-undo.
   define input parameter pv_where as character no-undo.

   define variable hChild as handle no-undo.
   define variable x as integer.

   create x-noderef hChild no-error.

   do x = 1 to hParent:num-children:

      hParent:get-child(hChild,x).

      if hParent:name = pv_where
      then do:
         hParent:append-child(hLine).         
      end.

      run p_attach_line(hChild,hLine,pv_where).

   end.

   delete object hChild.
end procedure.

procedure p_add_field:
   define input parameter hParent as handle no-undo.
   define input parameter hField as handle no-undo.
   define input parameter pv_field as character no-undo.
   define input parameter pv_text as character no-undo.

   define variable hText as handle.

   create x-noderef hText.

   /*ADD NODE AND DIRECT VALUE*/
   hParent:create-node(hField,pv_field,"ELEMENT").
   hParent:create-node(hText,"","TEXT").
   hField:append-child(hText).
   hText:node-value = pv_text.

   delete object hText.
end procedure.

procedure p_replace_domain:
   define input parameter hParent as handle no-undo.
   define input parameter hLine as handle no-undo.
   define input parameter pv_where as character no-undo.

   define variable hChild as handle no-undo.
   define variable x as integer.

   create x-noderef hChild no-error.

   do x = 1 to hParent:num-children:

      hParent:get-child(hChild,x).

      /**************
      if hParent:name = pv_where
      then do:
         MESSAGE hParent:name. PAUSE.
      end.
      ************/

      run p_replace_domain(hChild,hLine,pv_where).

   end.

   delete object hChild.
end procedure.

function f_add_slash returns character (input fv_dir as character):
   return
      if length(fv_dir) > 0
      and substr(fv_dir,length(fv_dir)) <> "/"
      then fv_dir + "/"
      else fv_dir
   .
end function.

function f_get_code_mstr returns character
   (input fv_fld as character, input fv_value as character):

   def var flv_char as character no-undo.

   for each code_mstr no-lock
   where code_domain = global_domain
   and code_fldname = fv_fld
   and code_value begins fv_value:
      flv_char = flv_char + trim(code_cmmt).
   end.

   return flv_char.

END FUNCTION.
   
FUNCTION qdoc_date RETURNS CHARACTER    
  (input indate as date):
  
  DEFINE VARIABLE strdate AS CHARACTER  NO-UNDO. 
  
  strdate = STRING(YEAR(indate), "9999") + "-" + STRING(Month(indate) ,"99" ) + "-" + STRING(DAY(indate), "99").
  
  RETURN strdate.

end function.
